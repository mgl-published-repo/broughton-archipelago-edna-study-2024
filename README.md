The code and data in this repository can be used to replicate the analysis and figures from:

Infectious agent release and Pacific salmon exposure at Atlantic salmon farms revealed by environmental DNA

by Bass et al 2024