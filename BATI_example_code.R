
library(tidyverse) #
library(glmmTMB) #
#library(cowplot)
library(readxl) #
library(lubridate) #
library(ggpubr) #
#library(gridExtra)
library(meta) #
library(DHARMa) #


#### import eDNA data ####
eDNA<-read_xlsx("bati_eDNA_data.xlsx",sheet="eDNA")
# set to PDT
eDNA$date = force_tz(eDNA$date,tzone="US/Pacific")

#### remove unused salmon assays 
# these were alternate assays for the pacific salmon species but these didn't work as well
# as those we ultimately used - also remove HKG columns which are irrelevant
assays_to_remove<-c("hkg_copy","Ongo_COI3_copy","Ongo_CYTB_copy","Onke_COI_copy",
                    "Onke_CYTB_copy","hkg_ct", "Ongo_COI3_ct", "Ongo_CYTB_ct",
                    "Onke_COI_ct", "Onke_CYTB_ct")
eDNA<-eDNA %>%
  dplyr::select(-all_of(assays_to_remove))

#### remove singleton eDNA molecular data 
# Only detections in duplicate (PCR duplicates) are kept in so remove singleton data here
st_ct = match("ae_sal_ct",colnames(eDNA))
end_ct = match("ye_ruc_ct",colnames(eDNA))
ct_copy_diff = st_ct - match("ae_sal_copy",colnames(eDNA))

# loop to remove singleton data from Ct and copy
for(i in st_ct:end_ct){
  singleton<-which(eDNA[,i]>40 & eDNA[,i]<999)
  if(length(singleton>0)){
    eDNA[singleton,i]<-999
    eDNA[singleton,i-ct_copy_diff]<-0
  }
}

#### account for field blank contamination 
# remove assays at higher Ct from same day and location as positive field blank

# identify positive blanks
pos_blanks<-eDNA %>%
  filter(sample_type=="Negative Control") %>%
  pivot_longer(cols=all_of(st_ct:end_ct)) %>%
  filter(value<40)

# replace positives and keep track
replaced_positives<-NULL

for(i in 1:nrow(pos_blanks)){
  pos_blank_ct<-pos_blanks$value[i]
  assay<-pos_blanks$name[i]
  location<-pos_blanks$location[i]
  date<-pos_blanks$date[i]
  assay_ct<-which(colnames(eDNA)==assay)
  
  to_replace<-which(eDNA$location==location & eDNA$date==date & 
                      eDNA$sample_type!="Negative Control" & eDNA[,assay_ct]>=pos_blanks$value[i])
  
  replaced<-data.frame("cts_replaced"=if(length(to_replace)>0){
    as.numeric(unlist(c(eDNA[to_replace,assay_ct])))}else{NA},
    "pos_blank_ct"=pos_blank_ct,
    "assay"=assay,
    "loc"=location,
    "date"=date)
  
  replaced_positives<-rbind(replaced_positives,replaced)
  # replace Cts with 999
  eDNA[to_replace,assay_ct] <- 999
  # replace copy nums with 0
  eDNA[to_replace,assay_ct - ct_copy_diff] <- 0
}

# this shows what was replaced
replaced_positives %>%
  filter(cts_replaced!=999,
         !is.na(cts_replaced))%>%
  arrange(assay,loc,date)

#### add lysis buffer type used 
# purelink and invitrogen are the same
exdat<-read_xlsx("bati_eDNA_data.xlsx",sheet="extraction")  
exdat$method<-NA
exdat$method[which(grepl("Purelink",exdat$extraction_method))]<-"purelink"
exdat$method[which(grepl("Invitrogen",exdat$extraction_method))]<-"purelink"
exdat$method[which(grepl("ReBead",exdat$extraction_method))]<-"rebead"
eDNA$ext_meth<-exdat$method[match(eDNA$dfo_id,exdat$dfo_id)]

# there are 22 samples with no extraction buffer info but can figure what was used based on date
# because purelink was used until April 2022
eDNA$ext_meth[which(is.na(eDNA$ext_meth) & substr(eDNA$date,1,4)<2023)]<-"purelink"
eDNA$ext_meth[which(is.na(eDNA$ext_meth) & substr(eDNA$date,1,4)==2023)]<-"rebead"

# prepare eDNA data
# this pipe averages sample replicates
# if positive in at least one replicate, considered positive
# nested dataset will be used below for presence/absence models
eDNA_PA<-eDNA %>%
  filter(!sample_type %in% c("Negative Control","Flush Sample"), 
         !location %in% c("Dalrymple Creek","Ocean Farms")) %>%
  mutate(mo_yr = format(date,format="%b-%Y"),
         mo_yr = factor(mo_yr, levels=c("Oct-2021","Nov-2021","Dec-2021","Jan-2022","Feb-2022","Mar-2022",
                                        "Apr-2022","May-2022","Jun-2022","Jul-2022","Aug-2022","Sep-2022",
                                        "Oct-2022","Nov-2022","Dec-2022","Jan-2023","Feb-2023")),
         sample = ifelse(sample_type=="Net Pen/Tank","farm",ifelse(sample_type=="Transect" & production_status %in% c("Broodstock", "Harvest","Productive/Active"), "farm", "inactive")),
         sample = factor(sample,levels=c("inactive","farm"))) %>%
  pivot_longer(cols=match("ae_sal_ct",colnames(.)):match("ye_ruc_ct",colnames(.)),names_to="assay",values_to = "ct_final") %>%
  group_by(location,date,mo_yr,sample_type,sample,net_pen_number,transect_direction,ext_meth,assay) %>%
  # get mean of eDNA replicates, if only one positive use that one 
  summarise(rep_mean = ifelse(mean(ct_final, na.rm=TRUE)>40,mean(ct_final[which(ct_final<=40)],na.rm=TRUE),mean(ct_final, na.rm=TRUE))) %>%
  group_by(location,date,mo_yr,sample,ext_meth,assay) %>%
  summarise(pos = length(which(!is.na(rep_mean))),      
            neg = length(which(is.na(rep_mean)))) %>%
  group_by(assay) %>%
  nest()


#### import S. salar tissue data ####
fish<-read_xlsx("bati_eDNA_data.xlsx",sheet="salar_tissue")
fish$sample_date_time= force_tz(fish$sample_date_time,tzone="US/Pacific")

# remove singleton data from fish data (pathogens must be detected in duplicate)
st_ct = match("ae_sal_ct",colnames(fish))
end_ct = match("ye_ruc_ct",colnames(fish))
ct_copy_diff = st_ct - match("ae_sal_copy",colnames(fish))

# loop to remove singleton data from Ct and copy
for(i in st_ct:end_ct){
  singleton<-which(fish[,i]>40 & fish[,i]<999)
  if(length(singleton>0)){
    fish[singleton,i]<-999
    fish[singleton,i-ct_copy_diff]<-0
  }
}

# prepare fish data
fish2<-fish %>%
  mutate(mo_yr=format(sample_date_time,format="%b-%Y"))%>%
  pivot_longer(cols=match("ae_sal_copy",colnames(.)):match("ye_ruc_copy",colnames(.)),names_to="assay",values_to = "copies")%>%
  mutate(copies = ifelse(copies<0,0,copies)) 

#### calculate prevalence across all samples ####
# import assay names
assay_names<-read_xlsx("bati_eDNA_data.xlsx",sheet="assays") 
# create prevalence data frame
prevalence<-data.frame(assay=substr(unique(eDNA_PA$assay),1,nchar(unique(eDNA_PA$assay))-3))
prevalence$name<-assay_names$pathogen[match(prevalence$assay,assay_names$abbrev2)]
prevalence$grouping<-assay_names$grouping[match(prevalence$assay,assay_names$abbrev2)]
fish2$assay<-substr(fish2$assay,1,nchar(fish2$assay)-5)

fish_prev<-fish2 %>%
  group_by(assay) %>%
  summarise(prev = round((length(which(copies>0))/length(!is.na(copies))) * 100,1))

prevalence$S.salar <- fish_prev$prev[match(prevalence$assay,fish_prev$assay)]

prev_farm<-unnest(eDNA_PA,cols = c(data)) %>%
  group_by(assay) %>%
  summarise(prev=round((sum(pos)/sum(pos,neg))*100,1))

prevalence$active_farm<-prev_farm$prev[match(prevalence$assay,substr(prev_farm$assay,1,nchar(prev_farm$assay)-3))]

prev_edna_buff<-unnest(eDNA_PA,cols = c(data)) %>%
  filter(sample=="farm") %>%
  group_by(ext_meth,assay) %>%
  summarise(prev=round((sum(pos)/sum(pos,neg))*100,1))

purelink<-prev_edna_buff%>%
  filter(ext_meth=="purelink")

rebead<-prev_edna_buff%>%
  filter(ext_meth=="rebead")

prevalence$purelink<-purelink$prev[match(prevalence$assay,substr(purelink$assay,1,nchar(purelink$assay)-3))]

prevalence$rebead<-rebead$prev[match(prevalence$assay,substr(rebead$assay,1,nchar(rebead$assay)-3))]

prev_inac<-unnest(eDNA_PA, cols = c(data)) %>%
  filter(sample=="inactive") %>%
  group_by(assay) %>%
  summarise(prev=round((sum(pos)/sum(pos,neg))*100,1))

prevalence$inactive<-prev_inac$prev[match(prevalence$assay,substr(prev_inac$assay,1,nchar(prev_inac$assay)-3))]

prevalence<-prevalence %>%
  arrange(grouping,name)

#### manuscript Table 1 #### 
View(prevalence)

####  manuscript Figure 4 ####
transfers<-read_xlsx("bati_eDNA_data.xlsx",sheet="fish_transfers",.name_repair = "universal") 
transfers$location<-transfers$farm
transfers<-transfers[-1,]

# prepare eDNA data for plot
eDNA_c<-eDNA %>%
  filter(!sample_type %in% c("Negative Control","Flush Sample"), 
         !location %in% c("Dalrymple Creek","Ocean Farms")) %>%
  mutate(mo_yr = format(date,format="%b-%Y"),
         mo_yr = factor(mo_yr, levels=c("Oct-2021","Nov-2021","Dec-2021","Jan-2022","Feb-2022","Mar-2022",
                                        "Apr-2022","May-2022","Jun-2022","Jul-2022","Aug-2022","Sep-2022",
                                        "Oct-2022","Nov-2022","Dec-2022","Jan-2023","Feb-2023")),
         sample = ifelse(sample_type=="Net Pen/Tank","farm",ifelse(sample_type=="Transect" & production_status %in% c("Broodstock", "Harvest","Productive/Active"), "farm", "inactive")),
         sample = factor(sample,levels=c("inactive","farm"))) %>%
  pivot_longer(cols=match("ae_sal_ct",colnames(.)):match("ye_ruc_ct",colnames(.)),names_to="assay",values_to = "ct_final") %>%
  group_by(location,date,mo_yr,sample_type,sample,net_pen_number,transect_direction,ext_meth,assay) %>%
  # get mean of eDNA replicates, if only one positive use that one 
  summarise(rep_mean = ifelse(mean(ct_final, na.rm=TRUE)>40,mean(ct_final[which(ct_final<=40)],na.rm=TRUE),mean(ct_final, na.rm=TRUE)))  %>%
  pivot_wider(values_from = rep_mean,names_from = assay) %>%
  mutate(T.maritimum = ifelse(is.na(te_mar_ct),41,te_mar_ct),
         O.tshawytscha = ifelse(is.na(Onts_COI_ct),41,Onts_COI_ct))

# prepare fish data for plot
fish_c<-fish %>%
  filter(sample_date_time < as.POSIXct("2023-04-01"),
         !farm %in% c("Dalrymple Creek","Ocean Farms")) %>%
  mutate(S.salar = ifelse(te_mar_ct==999,41,te_mar_ct),
         location = farm,
         date = sample_date_time,
         mo_yr = format(date,format="%b-%Y"),
         mo_yr = factor(mo_yr, levels=c("Oct-2021","Nov-2021","Dec-2021","Jan-2022","Feb-2022","Mar-2022",
                                        "Apr-2022","May-2022","Jun-2022","Jul-2022","Aug-2022","Sep-2022",
                                        "Oct-2022","Nov-2022","Dec-2022","Jan-2023","Feb-2023")))

p1<-eDNA_c %>%
  filter(!location %in% c("Cecil Island","Larsen Island","Port Elizabeth Harbour","Wicklow Point")) %>%
  ggplot(aes(x=date,y=T.maritimum)) +
  geom_jitter(data=fish_c,aes(x=date,y=S.salar,color="#009E73"),alpha=0.2,size=2,height=0,width=10^5.5)+
  geom_jitter(aes(color="#E69F00"),alpha=0.2,size=2,height=0,width=10^5.5) +
  geom_jitter(aes(date,O.tshawytscha,color="#0072B2"),alpha=0.2,size=2,height=0,width=10^5.5)+
  geom_vline(data=transfers[!transfers$farm %in% c("Cecil Island","Larsen Island","Port Elizabeth Harbour","Wicklow Point"),],
             aes(xintercept=Start.Date),color="black",linetype=2) +
  geom_vline(data=transfers[!transfers$farm %in% c("Cecil Island","Larsen Island","Port Elizabeth Harbour","Wicklow Point"),],
             aes(xintercept=harvest.date),color="#D55E00",linetype=2) +
  facet_wrap(~location,nrow=4) + 
  theme_bw() + ggtitle("Active farms") +
  theme(axis.title.x = element_blank(),
        legend.position = c(0.75,0.12)) +
  ylab("Cycle threshold (abundance)") +
  scale_y_reverse(limits=c(41,10)) +
  scale_color_identity(name = "sample type",
                       labels=c("O. tshawytscha eDNA","T. maritimum in \n S. salar tissue","T. maritimum eDNA"),
                       guide = "legend" )

p2<-eDNA_c %>%
  filter(location %in% c("Cecil Island","Larsen Island","Port Elizabeth Harbour","Wicklow Point")) %>%
  ggplot(aes(x=date,y=T.maritimum)) +
  geom_jitter(color="#E69F00",alpha=0.2,size=2,height=0,width=10^5.5) +
  geom_jitter(aes(date,O.tshawytscha),color="#0072B2",alpha=0.2,size=2,height=0,width=10^5.5)+
  geom_vline(data=transfers[transfers$farm %in% c("Cecil Island","Larsen Island","Port Elizabeth Harbour","Wicklow Point"),],
             aes(xintercept=harvest.date),color="#D55E00",linetype=2) +
  facet_wrap(~location,nrow=4) +
  theme_bw() + ggtitle("Inactive sites") +
  theme(axis.title = element_blank()) +
  scale_y_reverse(limits=c(41,10))

ggarrange(p1,p2,widths = c(2,1))


#### presence/absence model ####
# GLMM with AR1 
# run for salmon, marine fish, and all IAs

# presence/absence model formula
# binomial model, "sample" is variable of interest (active vs inactive farms)
# ext_meth controls for differences due to lysis buffer
# random intercepts for month and location
# AR1 for site specific temporal autocorrelation
pres_abs_ar1_mod<-function(df){glmmTMB(cbind(pos,neg) ~ sample + ext_meth +
                                         (1|mo_yr) + (1|location) +
                                         ar1(mo_yr + 0|location), 
                                       family=binomial, data=df)}

# run separate model for each pathogen
pa_mods<-eDNA_PA %>%
  mutate(model = purrr::map(data,pres_abs_ar1_mod)) 

# can see several models not converging 
warnings()

# identify non-converging models to remove below
non_converge<-c("ae_sal_ct","Lamprey_ct","Pacificod_ct","spav-1_ct",
                "spav-2_ct","surf_Smelt_ct","vhsv_ct","vi_ang_ct",
                "vi_sal_ct","ye_ruc_ct","surf Smelt_ct","ne_per_ct",
                "pa_pse_ct","sch_ct","le_sa_ct")

# model diagnostics
for(i in 1:49){
  fit_res<-simulateResiduals(pa_mods$model[[i]])
  windows()
  plot(fit_res)
  mtext(pa_mods$assay[i])
}

# extract coefficients for models that converged
pa_mods2<-pa_mods %>%
  filter(!assay %in% non_converge) %>%
  mutate(results = purrr::map(model,broom.mixed::tidy)) %>%
  unnest(results) %>%
  filter(term=="samplefarm")

# can have a look at how lysis buffer impacts likelihood of detection
lysis_effect<-pa_mods %>%
  filter(!assay %in% non_converge) %>%
  mutate(results = purrr::map(model,broom.mixed::tidy)) %>%
  unnest(results) %>%
  filter(term=="ext_methrebead")

lysis_effect%>%
  ggplot( aes(x=estimate,y=reorder(assay,estimate))) +
  geom_linerange(aes(xmin=estimate-2.58*std.error, xmax=estimate+2.58*std.error)) +
  geom_point(size=2) + geom_vline(xintercept = 0,color=2) + theme_bw() +
   theme(axis.title.y = element_blank()) +
  ggtitle("A) lysis buffer effect")

# add assay names for plots
pa_mods2$grouping<-assay_names$grouping[match(substr(pa_mods2$assay,1,nchar(pa_mods2$assay)-3),
                                              assay_names$abbrev2)]

pa_mods2$abb_name<-assay_names$abb_name[match(substr(pa_mods2$assay,1,nchar(pa_mods2$assay)-3),
                                              assay_names$abbrev2)]
#### manuscript figure 5 ####
salmon<-pa_mods2%>%
  filter(grouping=="salmonid") %>%
  ggplot( aes(x=exp(estimate),y=reorder(abb_name,estimate))) +
  geom_linerange(aes(xmin=exp(estimate-1.96*std.error), xmax=exp(estimate+1.96*std.error))) +
  geom_point(size=2) + geom_vline(xintercept = 1,color=2) + theme_bw() +
  coord_cartesian(xlim=c(0.5,500)) + 
  theme(axis.title.y = element_blank(),axis.title.x = element_blank()) +
  scale_x_log10()+
  ggtitle("A) salmonids")

mfish<-pa_mods2%>%
  filter(grouping=="marine_fish") %>% 
  ggplot( aes(x=exp(estimate),y=reorder(abb_name,estimate))) + 
  geom_linerange(aes(xmin=exp(estimate-1.96*std.error), xmax=exp(estimate+1.96*std.error))) +
  geom_point(size=2) + geom_vline(xintercept = 1,color=2) + theme_bw() +
  coord_cartesian(xlim=c(0.5,500)) + 
  theme(axis.title.y = element_blank(), axis.title.x = element_blank()) +
  scale_x_log10()+
  ggtitle("B) marine fish")

pathogens<-pa_mods2%>%
  filter(grouping=="pathogen") %>%
  ggplot( aes(x=exp(estimate),y=reorder(abb_name,estimate))) + 
  geom_linerange(aes(xmin=exp(estimate-1.96*std.error), xmax=exp(estimate+1.96*std.error))) +
  geom_point(size=2) + geom_vline(xintercept = 1,color=2) + theme_bw() +
  coord_cartesian(xlim=c(0.5,500)) + 
  theme(axis.title.y = element_blank(),axis.title.x = element_blank()) +
  scale_x_log10()+
  ggtitle("C) pathogens")

annotate_figure(ggarrange(ggarrange(salmon,mfish,ncol=1),pathogens,nrow=1), bottom="odds ratio")

#### compare Shea et al 2020 results ####
# data from https://royalsocietypublishing.org/doi/full/10.1098/rspb.2020.2010
shea_dat<-read_xlsx("BATI_edna_data.xlsx",sheet="Shea_Supp_Data_File")

eDNA_shea<-shea_dat %>%
  filter(Binary_Microparasite_Detection!="NA") %>%
  group_by(Site,Year,Active,Microparasite_Spp_Names) %>%
  summarise(pos = length(which(Binary_Microparasite_Detection==1)),     
            neg = length(which(Binary_Microparasite_Detection==0))) %>%
  group_by(Microparasite_Spp_Names) %>%
  nest()

# most similar model structure we could create
# no AR1 autocorr, Shea samples from just a few months each summer
pres_abs_shea<-function(df){glmmTMB(cbind(pos,neg) ~ Active +
                                      (1|Year) + (1|Site),
                                    family=binomial, data=df)}


# run separate model for each pathogen
shea_mods<-eDNA_shea %>%
  mutate(model = purrr::map(data,pres_abs_shea)) 

# a bunch of models didn't converge so will remove
non_converge<-c("ASCV","C_B_cysticola",
                "I_hoferi","K_thyrsites",
                "P_pseudobranchicola","PRV","SPAV_2","V_anguillarum","V_salmonicida")

shea_mods2<-shea_mods %>%
  filter(!Microparasite_Spp_Names %in% non_converge)%>%
  mutate(results = purrr::map(model,broom.mixed::tidy)) %>%
  unnest(results) %>%
  filter(term=="Active")

#### compare release estimates to consequence from literature ####
# import literature values
lit_vals<-read_xlsx("BATI_edna_data.xlsx",sheet="lit_scores")
lit_vals$total<-rowSums(lit_vals[,2:4])

pa_mods2$assay2<-substr(pa_mods2$assay,1,nchar(pa_mods2$assay)-3)

pa_mods2$study<-"bass et al"
#add in Shea mods
# names need to match our study
shea_mods2$assay2<-c("sch","ctv-2","pa_ther","fa_mar","fl_psy","mo_vis","pisck_sal","te_mar","env","ver","ye_ruc","pa_kab","p-narnav")
shea_mods2$study<-"shea et al"
shea_mods3<-shea_mods2 %>%
  filter(assay2 %in% pa_mods2$assay2)

results<-rbind(pa_mods2[,c(14,15,8:11)],shea_mods3[,c(12,13,8:11)])

matdat<-merge(results,lit_vals,by=1)

# fix some pathogen abbrevs
matdat$assay2[which(matdat$assay2=="Te_fin_gyrB")]<-"te_fin"
matdat$assay2[which(matdat$assay2=="ic_hof")]<-"icp_spp"
matdat$assay2[which(matdat$assay2=="lo_sal")]<-"lo_spp"

# order pathogens by lit score and then model estimate
matdat_ord<-matdat %>%
  arrange(total,estimate) %>%
  mutate(order = 1:nrow(matdat))

#### manuscript figure 7 ####
ggplot(matdat_ord,aes(x=exp(estimate),y=order, label=assay2,xmin=exp(estimate-1.96*std.error),xmax=exp(estimate+1.96*std.error))) + 
 # annotation_custom(grob = g, xmin = -Inf, xmax = Inf, ymin = -Inf, ymax = Inf) + 
  annotate("rect", xmin=exp(0.42), xmax=exp(1.94), ymin=-Inf, ymax=Inf, alpha=0.1, fill="#0072B2")+
  geom_vline(aes(xintercept=exp(1.18)),linetype=2,color="#0072B2") +
  annotate("rect", xmin=exp(0.84), xmax=exp(2.09), ymin=-Inf, ymax=Inf, alpha=0.1, fill="#E69F00")+
  geom_vline(aes(xintercept=exp(1.46)),linetype=2,color="#E69F00") +
  geom_vline(aes(xintercept=1),linetype=1,linewidth=0.5) +
  geom_hline(yintercept=c(5.5,8.5,19.5,21.5,22.5,29.5),linetype=3) +
  geom_pointrange(linetype=1) + #position = position_jitter(seed = 2,width=0.1,height=0.4)
  geom_point(pch=21,aes(fill=study),size=3) + #position = position_jitter(seed = 2,width=0.1,height=0.4),size=3
  ggrepel::geom_text_repel(aes(color=study,size=lack_evidence),show.legend = TRUE) + 
  scale_size(name="Uncertainty",labels = c(0,1,2,3),range = c(7,3)) +
  scale_color_manual(values=c("#E69F00","#0072B2"))+
  scale_fill_manual(values=c("#E69F00","#0072B2"))+
   coord_cartesian( xlim=c(0.5,500), clip = "on") +#ylim = c(-0.5, 6.5),
  ylab("consequence of infection score (from literature)") + 
  xlab("pathogen release associated with farms (eDNA GLMM odds ratios)") +   theme_bw() +
  scale_y_continuous(breaks = c(3,7,14.5,20.5,22,27,32),labels = c(0,1,2,3,4,5,6))+
  theme(legend.position = c(0.9,0.2),
        axis.title = element_text(size=15),
        axis.text.y = element_text(size=13), 
        axis.text.x = element_text(size=13),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank()) +
    scale_x_log10()

#### estimate metanalytical mean for both studies ####
matdat$pathnames = assay_names$abb_name[match(matdat$assay2,assay_names$abbrev2)]

# identify pathogens from our study also tested by shea et al
overlap<-matdat[which(matdat$assay2 %in% matdat$assay2[which(matdat$study=="shea et al")] & matdat$study=="bass et al"),]
overlap$study = "from bass, overlapping shea"

matdat2<-rbind(matdat,overlap)

m<-metagen(data=matdat2, TE=estimate, seTE=std.error, byvar = study,
           studlab = pathnames, level = 0.95, method.tau="PM",
           level.comb = 0.95, comb.fixed = FALSE, 
           comb.random=TRUE, sm="SMD", prediction=FALSE,overall=FALSE)

#### Manuscript figure S1 ####
forest(m, sortvar=TE, subgroup=TRUE, fontsize=10, leftcols="studlab",
       rightlabs = c("CE","95% CI","Weight"),
       leftlabs=c("Infectious agent"), smlab = "Coefficient estimate",
       hetstat=FALSE, spacing = 0.6, col.diamond = "gray", col.random = "red",
       xlab = "CE for sample location (inactive vs active farm)",
       studlab = matdat2$pathnames, 
       col.square = ifelse(matdat2$study=="bass et al","#F8766D",
                           ifelse(matdat2$study=="shea et al","#00BFC4","#00BA38")))

#### directly compare estimates between studies ####
# make a plot that will compare DS to AB results 
# and run linear regression
ABmods<-pa_mods2[,c(14,15,8:11)]
DSmods<-shea_mods3[,c(12,13,8:11)]

compdat<-merge(ABmods,DSmods,by=1)

summary(lm(compdat$estimate.y ~ compdat$estimate.x))

#### manuscript Figure S2 ####
ggplot(compdat,aes(x=estimate.x, y=estimate.y)) +
  geom_abline(intercept = 0,slope=1,color=2) +
  geom_smooth(method="lm",linetype=2) +
  geom_linerange(aes(xmin=estimate.x-1.96*std.error.x, xmax=estimate.x+1.96*std.error.x),color="gray50") +
  geom_linerange(aes(ymin=estimate.y-1.96*std.error.y, ymax=estimate.y+1.96*std.error.y),color="gray50") +
  geom_point(size=4,pch=21,fill="lightblue") +
  #scale_x_log10() +
  #scale_y_log10(breaks=c(1,10,100)) +
  ggrepel::geom_text_repel(aes(label=assay2)) +
  theme_bw() +
  xlab("GLMM estimates from this study") +
  ylab("GLMM estimates from Shea et al. 2020")


#### pellet feed figure ####
feed<-read_xlsx("BATI_edna_data.xlsx",sheet="feed_results")

feed2<-feed %>%
   filter(!fluidgim_ID %in% c("FD0001","0.5FD0022")) %>%
  pivot_longer(cols=c(5:12,14), names_to = "assay",values_to = "ct_final")
 

eDNA_f<-eDNA %>%
  filter(!sample_type %in% c("Negative Control","Flush Sample"), 
         !location %in% c("Dalrymple Creek","Ocean Farms")) %>%
  mutate(mo_yr = format(date,format="%b-%Y"),
         mo_yr = factor(mo_yr, levels=c("Oct-2021","Nov-2021","Dec-2021","Jan-2022","Feb-2022","Mar-2022",
                                        "Apr-2022","May-2022","Jun-2022","Jul-2022","Aug-2022","Sep-2022",
                                        "Oct-2022","Nov-2022","Dec-2022","Jan-2023","Feb-2023")),
         sample = ifelse(sample_type=="Net Pen/Tank","active",ifelse(sample_type=="Transect" & production_status %in% c("Broodstock", "Harvest","Productive/Active"), "active", "inactive")),
         sample = factor(sample,levels=c("inactive","active"))) %>%
  pivot_longer(cols=match("ae_sal_ct",colnames(.)):match("ye_ruc_ct",colnames(.)),names_to="assay",values_to = "ct_final") %>%
  filter(assay %in% paste0(feed2$assay,"_ct")) %>%
  group_by(location,date,mo_yr,sample_type,sample,net_pen_number,transect_direction,ext_meth,assay) %>%
  # get mean of eDNA replicates, if only one positive use that one 
  summarise(rep_mean = ifelse(mean(ct_final, na.rm=TRUE)>40,mean(ct_final[which(ct_final<=40)],na.rm=TRUE),mean(ct_final, na.rm=TRUE))) %>%
  mutate(assay2 = substr(assay,1,nchar(assay)-3))

eDNA_f$abb_name<-assay_names$abb_name[match(eDNA_f$assay2,assay_names$abbrev2)]
eDNA_f$abb_name<-factor(eDNA_f$abb_name,levels=c("S. salar","O. nerka","Clupea spp.",
                                                 "Engraulis spp.","G. aculeatus",
                                                 "ENv","Ichthyophonus spp.","K. thyrsites",
                                                 "Loma spp."))

feed2$abb_name<-assay_names$abb_name[match(feed2$assay,assay_names$abbrev2)]

#### manuscript Figure S3 ####
ggplot(eDNA_f,aes(x=rep_mean,y=abb_name)) +
  scale_y_discrete(limits=rev) +
  scale_x_reverse() +
  geom_jitter(position=position_jitter(seed=1,height=0.4,width=0),aes(color=paste0(sample_type,sep="_",sample)),alpha=0.5) +
  geom_jitter(position=position_jitter(seed=1,height=0.2,width=0),data=feed2,shape=21,aes(x=ct_final,y=abb_name,fill=sample),size=4,stroke=2) +
  scale_fill_manual(breaks=c("feed_1","feed_2"),values=c("white","gray60")) +
  theme_bw() +
  theme(legend.position = "bottom") +
  guides(fill = guide_legend(ncol = 1), color = guide_legend(ncol=1)) +
  labs(col = "field sample\n type", fill="Feed sample", x = "PCR Cycle threshold (abundance)", y = "")

#### sea lice analysis ####
sealice<-read_xlsx("BATI_edna_data.xlsx",sheet="sea_lice")

sl2<-sealice %>%
  mutate(location = `Facility  Name`,
         datetime=`Pen Sample  Date`,
         mo_yr = as.factor(format(datetime,format="%b-%Y")),
         netpen = as.numeric(substr(`Pen  Number`,nchar(`Pen  Number`)-1,nchar(`Pen  Number`)))) %>%
  group_by(location, mo_yr, datetime, netpen) %>%
  summarise(avg_lep = mean(`Adult Motiles Per  Fish`,na.rm=TRUE),
            avg_cal = mean(`Caligus Motiles Per  Fish`,na.rm=TRUE))

eDNA_lice<-eDNA %>%
  filter(!sample_type %in% c("Negative Control","Flush Sample"), 
         !location %in% c("Dalrymple Creek","Ocean Farms")) %>%
  mutate(mo_yr = format(date,format="%b-%Y"),
         mo_yr = factor(mo_yr, levels=c("Oct-2021","Nov-2021","Dec-2021","Jan-2022","Feb-2022","Mar-2022",
                                        "Apr-2022","May-2022","Jun-2022","Jul-2022","Aug-2022","Sep-2022",
                                        "Oct-2022","Nov-2022","Dec-2022","Jan-2023","Feb-2023")),
         sample = ifelse(sample_type=="Net Pen/Tank","farm",ifelse(sample_type=="Transect" & production_status %in% c("Broodstock", "Harvest","Productive/Active"), "farm", "inactive")),
         sample = factor(sample,levels=c("inactive","farm"))) %>%
  pivot_longer(cols=match("ae_sal_ct",colnames(.)):match("ye_ruc_ct",colnames(.)),names_to="assay",values_to = "ct_final") %>%
  pivot_longer(cols=match("ae_sal_copy",colnames(.)):match("ye_ruc_copy",colnames(.)),names_to="assay_c",values_to = "calc_final") %>%
  filter(substr(assay,1,nchar(assay)-3) == substr(assay_c,1,nchar(assay_c)-5)) %>%
  group_by(location ,date,mo_yr,sample_type,sample,net_pen_number,transect_direction,ext_meth,assay) %>%
  # get mean of eDNA replicates, if only one positive use that one 
  mutate(dup_flag = ifelse(mean(ct_final, na.rm=TRUE)>40 & mean(ct_final, na.rm = TRUE)<999,1,0),
         greater = ifelse(calc_final==max(calc_final),1,0),
         remove = ifelse(dup_flag==1 & greater==0,1,0)) %>%
  filter(remove==0) %>%
  summarise(copies=mean(calc_final,na.rm=TRUE)) %>%
  filter(sample=="farm") %>%
  group_by(mo_yr,date,location,assay,ext_meth,net_pen_number) %>%
  summarise(log_avg_copies = log1p(mean(copies,na.rm=TRUE)),
            samp_size = n()) %>%
  group_by(mo_yr,date,location,assay, net_pen_number) %>%
  mutate(max_n = max(samp_size)) %>%
  filter(samp_size == max_n,
         assay %in% c("le_sa_ct","ca_cl_ct")) %>%
  pivot_wider(names_from=assay,values_from = log_avg_copies)

# different datasets have different names for farms so need to match them up
farm.key = data.frame(amd.name = unique(sl2$location), edna.name = c(unique(eDNA_lice$location)[c(6,2,3,1,4,7,5)],NA))

sl2$location = farm.key$edna.name[match(sl2$location,farm.key$amd.name)]

dat<-merge(sl2,eDNA_lice,by.x=c(1,2,4),by.y=c(3,1,5))

dat2<-dat %>%
  mutate(timediff=abs(as.numeric(difftime(datetime,date,units="days")))) %>%
  group_by(location,date) %>%
  mutate(min_diff = min(timediff)) %>%
  filter(min_diff==timediff)

dat2<-dat2 %>%
  mutate(mo_yr = factor(mo_yr, levels=c("Oct-2021","Nov-2021","Dec-2021","Jan-2022","Feb-2022",
                                        "Mar-2022","Apr-2022","May-2022","Jun-2022","Jul-2022","Aug-2022",
                                        "Sep-2022", "Oct-2022","Nov-2022","Dec-2022","Jan-2023","Feb-2023")))


# is likelihood of eDNA positive detection associated with AMD count?
cacl_binom<-glmmTMB(ca_cl_ct>0 ~ avg_cal + ext_meth +
                      (1|mo_yr) + (1|location) +
                      ar1(mo_yr + 0|location), 
                    family=binomial, data=dat2)

newdat<-data.frame(avg_cal=rep(seq(0,4,0.1),2), ext_meth=c(rep("purelink",41),rep("rebead",41)),mo_yr=NA,location=NA)
cacl_preds<-predict(cacl_binom,newdata = newdat,re.form=NA,type="link",se.fit = TRUE)
ccb<-cbind(newdat,cacl_preds$fit,cacl_preds$se.fit)
colnames(ccb)[c(2,5,6)]<-c("lysis_buffer","fit","std_err")

ccbinom<-ggplot(ccb,aes(avg_cal,plogis(fit),color=lysis_buffer,fill=lysis_buffer)) +
  geom_ribbon(aes(ymin=plogis(fit-1.96*std_err),ymax=plogis(fit+1.96*std_err)),alpha=0.5) +
  geom_line(linewidth=2) +
  scale_y_continuous(limits=c(0,1),expand=c(0,0)) +
  theme_bw() +
  ylab("probability of eDNA positive") +
  xlab("Industry count C. clemensi per fish ") +
  ggtitle("B)")

# same for leps
lesa_binom<-glmmTMB(le_sa_ct>0 ~ avg_lep + 
                      (1|mo_yr) + (1|location) +
                      ar1(mo_yr + 0|location), 
                    family=binomial, data=dat2[dat2$ext_meth=="purelink",])

newdat<-data.frame(avg_lep=rep(seq(0,5,0.1),2), ext_meth=c(rep("purelink",51),rep("purelink",51)),mo_yr=NA,location=NA)
lesa_preds<-predict(lesa_binom,newdata = newdat,re.form=NA,type="link",se.fit = TRUE)
lsb<-cbind(newdat,lesa_preds$fit,lesa_preds$se.fit)
colnames(lsb)[c(2,5,6)]<-c("lysis_buffer","fit","std_err")

lsbinom<-ggplot(lsb,aes(avg_lep,plogis(fit),color=lysis_buffer,fill=lysis_buffer)) +
  geom_ribbon(aes(ymin=plogis(fit-1.96*std_err),ymax=plogis(fit+1.96*std_err)),alpha=0.5) +
  geom_line(linewidth=2) +
  scale_y_continuous(limits=c(0,1),expand=c(0,0)) +
  theme_bw() +
  ylab("probability of eDNA positive") +
  xlab("Industry count L. salmonis per fish ") +
  ggtitle("C)")

# is AMD count and eDNA load correlated?
cacl_corr<-glmmTMB(ca_cl_ct ~ avg_cal + ext_meth +
                     (1|mo_yr) + (1|location) +
                     ar1(mo_yr + 0|location), 
                   family=gaussian, data=dat2)

newdat<-data.frame(avg_cal=rep(seq(0,4,0.1),2), ext_meth=c(rep("purelink",41),rep("rebead",41)),mo_yr=NA,location=NA)
cacl_preds<-predict(cacl_corr,newdata = newdat,re.form=NA,type="link",se.fit = TRUE)
ccc<-cbind(newdat,cacl_preds$fit,cacl_preds$se.fit)
colnames(ccc)[c(2,5,6)]<-c("lysis_buffer","fit","std_err")

cccorr<-ggplot(ccc,aes(avg_cal,fit,color=lysis_buffer,fill=lysis_buffer)) +
  geom_ribbon(aes(ymin=fit-1.96*std_err,ymax=fit+1.96*std_err),alpha=0.5) +
  geom_line(linewidth=2) +
  coord_cartesian(ylim=c(0,6.5)) +
  theme_bw() +
  ylab("Log copies C. clemensi eDNA") +
  xlab("Industry count C. clemensi per fish ") +
  geom_point(inherit.aes=FALSE,data=dat2,aes(x=avg_cal,y=ca_cl_ct,color=ext_meth),alpha=0.5,size=3) + ggtitle("D)")
# and for leps
lesa_corr<-glmmTMB(le_sa_ct ~ avg_lep + ext_meth +
                     (1|mo_yr) + (1|location) +
                     ar1(mo_yr + 0|location), 
                   family=gaussian, data=dat2)

newdat<-data.frame(avg_lep=rep(seq(0,5,0.1),2), ext_meth=c(rep("purelink",51),rep("rebead",51)),mo_yr=NA,location=NA)
le_sa_preds<-predict(lesa_corr,newdata = newdat,re.form=NA,type="link",se.fit = TRUE)
lsc<-cbind(newdat,le_sa_preds$fit,le_sa_preds$se.fit)
colnames(lsc)[c(2,5,6)]<-c("lysis_buffer","fit","std_err")

lscorr<-ggplot(lsc,aes(avg_lep,fit,color=lysis_buffer,fill=lysis_buffer)) +
  geom_ribbon(aes(ymin=fit-1.96*std_err,ymax=fit+1.96*std_err),alpha=0.5) +
  geom_line(linewidth=2) +
  coord_cartesian(ylim=c(0,5)) +
  theme_bw() +
  ylab("Log copies L. salmonis eDNA") +
  xlab("Industry count L. salmonis per fish ") +
  geom_point(inherit.aes=FALSE,data=dat2,aes(x=avg_lep,y=le_sa_ct,color=ext_meth),alpha=0.5,size=3) + ggtitle("E)")

avg_mots<-sl2 %>%
  pivot_longer(cols=5:6,names_to = "species",values_to="avg_motiles") %>%
  group_by(location,species,datetime) %>%
  summarise(avg_mot = mean(avg_motiles))

amd_counts<-sl2 %>%
  pivot_longer(cols=5:6,names_to = "species",values_to="avg_motiles") %>%
  ggplot(aes(datetime,avg_motiles,group=interaction(species,location),color=species)) +
  geom_point(alpha=0.5) +
  geom_line(inherit.aes=F,data=avg_mots,aes(x=datetime,y=avg_mot,group=interaction(species,location),color=species)) +  scale_color_manual(name="Copepod species",
                     values=c("orange","blue"),breaks=c("avg_cal","avg_lep"),
                     labels=c("C.clemensi","L.salmonis")) +
  theme_bw() +ggtitle("A)") +
  theme(legend.position = c(0.85,0.75),axis.title.x = element_blank()) +
  ylab("Industry count motiles per fish")

#### manuscript figure s4 ####
ggarrange(amd_counts,ggarrange(ccbinom,lsbinom,common.legend = TRUE,legend="bottom"),
          ggarrange(cccorr,lscorr,common.legend = TRUE,legend="none"),nrow=3)


